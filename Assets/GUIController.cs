﻿using System.Collections;
using Assets.Scripts.Managers;
using Assets.Scripts.PlayerScripts;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
    #region Serialized Fields
    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Slider healthSilder;

    [SerializeField]
    private Color healthStartColor;

    [SerializeField]
    private Color healthEndColor;

    [SerializeField]
    private float scalingMultiplier;

    [SerializeField]
    private float distanceFromPlayer = 10;

    [SerializeField]
    private Text endGameText;
    #endregion

    #region Private Fields
    private Image healthSliderFill;
    private Coroutine scaleTextCoroutine;
    private Player player;
    private Coroutine healthSliderFade;
    #endregion

    #region Unity Messages
    private void Start()
    {
        healthSliderFill = healthSilder.fillRect.GetComponent<Image>();
        player = GameManager.Instance.Player;
        player.OnPlayerDamage.AddListener(UpdateHealthValue);
    }

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, GetTargetPosition(), Time.deltaTime * 5);
        transform.LookAt(Camera.main.transform);
    }

    private void UpdateHealthValue()
    {
        healthSilder.value = player.GetPlayerHealth01;
        healthSliderFill.color = Color.Lerp(healthStartColor, healthEndColor, 1 - player.GetPlayerHealth01);
        if (healthSilder.value < 0.3f && healthSliderFade == null)
        {
            healthSliderFade = StartCoroutine(FadeHealthSliderFill(8));
        }
    }

    private void UpdateScoreText()
    {
        scoreText.text = GameManager.Instance.Score + "";
    }

    
    #endregion

    #region Public Methods
    public void UpdateEndGamePanel()
    {
        endGameText.text = $" Gameover! Score: {GameManager.Instance.Score}";
    }
    public void AnimateScoreHit()
    {
        UpdateScoreText();
        if (scaleTextCoroutine != null)
        {
            StopCoroutine(scaleTextCoroutine);
        }

        scaleTextCoroutine = StartCoroutine(AnimateScoreText(0.2f, 0.2f));
    }

    public void AnimateScoreKill()
    {
        UpdateScoreText();
        if (scaleTextCoroutine != null)
        {
            StopCoroutine(scaleTextCoroutine);
        }

        scaleTextCoroutine = StartCoroutine(AnimateScoreText(0.4f, 0.2f));
    }
    #endregion

    #region Private Methods
    private IEnumerator FadeHealthSliderFill(float fadeRate)
    {
        while (true)
        {
            Color currentColor = healthSliderFill.color;
            currentColor *= Mathf.Sin(Time.time * fadeRate) / 2f + 0.5f;
            healthSliderFill.color = currentColor;
            yield return null;
        }
    }

    private IEnumerator AnimateScoreText(float scaleValue, float time)
    {
        Vector3 startScale = Vector3.one * (1 + scaleValue) * scalingMultiplier;
        float startTime = time;
        while (time > 0)
        {
            scoreText.rectTransform.localScale = Vector3.Lerp(startScale, Vector3.one, 1 - time / startTime);
            time -= Time.deltaTime;
            yield return null;
        }

        scoreText.rectTransform.localScale = Vector3.one;
    }

    private Vector3 GetTargetPosition() => Vector3.ProjectOnPlane(Camera.main.transform.forward, Vector3.up).normalized * distanceFromPlayer;
    #endregion
}
﻿using System.Collections;
using Assets.Scripts.Enemies;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    [RequireComponent(typeof(GameObjectPool))]
    public class EnemyManager : Singleton<EnemyManager>
    {
        #region Serialized Fields
        [SerializeField]
        private float spawnDistance = 5;

        [SerializeField, Range(0, 100)]
        private int startingNumberOfEnemies = 10;

        [SerializeField, Range(1, 1000)]
        private int maximumNumberOfEnemies = 100;

        [SerializeField, Range(0.1f, 5)]
        private float delayBetweenSpawns = 1f;
        #endregion

        private GameObjectPool enemyPool;
        #region Unity Messages
        private void Start()
        {
            enemyPool = GetComponent<GameObjectPool>();
            GameManager.Instance.OnGameStart.AddListener(SpawnInitialEnemies);
            GameManager.Instance.OnGameStart.AddListener(EnableEnemySpawning);
        }
        #endregion

        #region Private Methods
        private void SpawnInitialEnemies()
        {
            for (int i = 0; i < startingNumberOfEnemies; i++)
            {
                SpawnEnemy();
            }
        }

        private void EnableEnemySpawning()
        {
            StartCoroutine(AttemptEnemySpawn());
        }

        private IEnumerator AttemptEnemySpawn()
        {
            while (true)
            {
                if (enemyPool.ActiveInstanceCount < maximumNumberOfEnemies)
                {
                    SpawnEnemy();
                }
                yield return new WaitForSeconds(delayBetweenSpawns);
            }
        }

        private void SpawnEnemy()
        {
            Enemy enemyInstance = (Enemy)enemyPool.GetFromPool();
            enemyInstance.transform.position = GetRandomEnemyPosition();
            enemyInstance.Launch();
        }

        private Vector3 GetRandomEnemyPosition()
        {
            Vector2 offset = Random.insideUnitCircle.normalized;
            return GameManager.Instance.PlayerPosition + new Vector3(offset.x, 0, offset.y) * spawnDistance;
        }
        #endregion
    }
}
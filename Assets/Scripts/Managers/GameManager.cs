﻿using Assets.Scripts.PlayerScripts;
using Assets.Scripts.Util;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.Managers
{
    public class GameManager : Singleton<GameManager>
    {
        #region Serialized Fields
        [SerializeField]
        private Player player;
        [SerializeField]
        private UnityEvent onGameStart;
        [SerializeField]
        private UnityEvent onEnemyHit;
        [SerializeField]
        private UnityEvent onEnemyKill;
        [SerializeField]
        private UnityEvent onGameEnd;
        #endregion

        #region Private Fields
        private PlayerInput playerInput;
        private int score = 0;
        #endregion

        #region Public Properties
        public Player Player => player ?? FindObjectOfType<Player>();
        public Vector3 PlayerPosition => player.transform.position;
        public UnityEvent OnGameStart => onGameStart;
        public UnityEvent OnGameEnd => onGameEnd;
        public UnityEvent OnEnemyHit => onEnemyHit;
        public UnityEvent OnEnemyKill => onEnemyKill;
        public int Score => score;
        #endregion

        #region Unity Messages
        private void Start()
        {
            if (player == null)
            {
                player = FindObjectOfType<Player>();
            }
            playerInput = player?.GetComponent<PlayerInput>();
            playerInput?.OnUserTap.AddListener(StartGame);
        }

        public void StartGame()
        {
            Debug.Log("start Game");
            onGameStart.Invoke();
            playerInput.OnUserTap.RemoveAllListeners();
            playerInput.OnUserTap.AddListener(player.Shoot);
        }
        #endregion

        public void AddHit(int hitScore)
        {
            AddScore(hitScore);
            onEnemyHit.Invoke();
        }

        public void AddKill(int killScore)
        {
            AddScore(killScore);
            onEnemyKill.Invoke();
        }

        private void AddScore(int scoreToAdd)
        {
            score = Score + scoreToAdd;
        }

        public void EndGame()
        {
            Time.timeScale = 0;
            playerInput.OnUserTap.RemoveListener(player.Shoot);
            onGameEnd.Invoke();
        }
    }
}
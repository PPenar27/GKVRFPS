﻿using System.Collections;
using Assets.Scripts.Enemies;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.Gameplay
{
    public class Bullet : MonoBehaviour, IPoolable
    {
        #region Serialized Fields
        [SerializeField]
        private float bulletSpeed;

        [SerializeField]
        private float bulletDamage;

        [SerializeField]
        private ParticleSystem impactEffect;
        #endregion

        #region Private Fields
        private GameObjectPool sourcePool;
        private bool movementEnabled = true;
        #endregion

        #region Unity Messages
        private void FixedUpdate()
        {
            if (movementEnabled)
            {
                transform.position += transform.forward * bulletSpeed * Time.fixedDeltaTime;
            }
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            if (otherCollider.CompareTag("Enemy"))
            {
                GetComponent<Renderer>().enabled = false;
                otherCollider.GetComponent<Enemy>()?.DealDamage(bulletDamage);
                movementEnabled = false;
                LaunchImpactEffect();
            }
        }

        public void ResetProperties()
        {
            GetComponent<TrailRenderer>().Clear();
        }
        #endregion

        #region Public Methods
        public void Launch()
        {
            gameObject.SetActive(true);
            movementEnabled = true;
        }

        public void SetSourcePool(GameObjectPool pool)
        {
            sourcePool = pool;
        }

        public void ReturnToPool()
        {
            sourcePool.ReturnToPool(this);
        }

        public GameObject GetGameObject() => gameObject;
        #endregion

        #region Private Methods
        private void LaunchImpactEffect()
        {
            if (impactEffect != null)
            {
                impactEffect.Play();
                StartCoroutine(DisableAfterEffectEnd());
            }
        }

        private IEnumerator DisableAfterEffectEnd()
        {
            while (impactEffect.isPlaying)
            {
                yield return null;
            }

            ReturnToPool();
        }
        #endregion
    }
}
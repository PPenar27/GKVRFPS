﻿using System.Collections;
using Assets.Scripts.Interfaces;
using Assets.Scripts.Managers;
using Assets.Scripts.Util;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace Assets.Scripts.Enemies
{
    [RequireComponent(typeof(NavMeshAgent))]
    public class Enemy : MonoBehaviour, IPoolable
    {
        #region Serialized Fields
        [SerializeField]
        private EnemyType enemyType;

        [SerializeField]
        private Transform targetTransform;

        [SerializeField]
        private float health;

        [SerializeField]
        private UnityEvent onLaunch;

        [SerializeField]
        private UnityEvent onDamage;

        [SerializeField]
        private UnityEvent onDeath;
        #endregion

        #region Private Fields
        private NavMeshAgent navMeshAgent;
        private Vector3? targetPosition;
        private GameObjectPool sourcePool;
        private float minDistanceFromPlayer = 5f;
        #endregion

        #region Unity Messages
        private void Awake()
        {
            navMeshAgent = GetComponent<NavMeshAgent>();
            navMeshAgent.speed = enemyType.MovementSpeed;
        }

        private void OnEnable()
        {
            StartCoroutine(AttemptToSetTarget());
        }

        public void ResetProperties()
        {
            health = enemyType.MaxHealth;
        }

        private void Update()
        {
            if (targetPosition != null && Vector3.Distance(targetPosition.Value, transform.position) < minDistanceFromPlayer)
            {
                targetPosition = null;
                navMeshAgent.destination = transform.position;
                GetComponent<Rigidbody>().isKinematic = true;;
            }
        }
        #endregion

        #region Public Methods
        public void Launch()
        {
            gameObject.SetActive(true);
            if (targetTransform == null)
            {
                targetTransform = GameManager.Instance.Player.transform;
            }

            onLaunch.Invoke();
        }

        public void DealDamage(float bulletDamage)
        {
            GameManager.Instance.AddHit(enemyType.ScorePerHit);
            health -= bulletDamage;
            if (health <= 0)
            {
                GameManager.Instance.AddKill(enemyType.ScorePerKill);
                EnemyDeath();
            }
        }

        public void SetSourcePool(GameObjectPool pool)
        {
            sourcePool = pool;
        }

        public void ReturnToPool()
        {
            sourcePool.ReturnToPool(this);
        }

        public GameObject GetGameObject() => gameObject;
        #endregion

        #region Private Methods
        private void EnemyDeath()
        {
            ReturnToPool();
        }

        private IEnumerator AttemptToSetTarget()
        {
            if (targetTransform != null && Vector3.Distance(targetTransform.position, transform.position) < minDistanceFromPlayer)
            {
                yield break;
            }

            while (targetPosition == null)
            {
                if (targetTransform != null && navMeshAgent.isOnNavMesh)
                {
                    targetPosition = targetTransform.position;
                    navMeshAgent.SetDestination(targetTransform.position);
                }

                yield return null;
            }
        }
        #endregion
    }
}
﻿using UnityEngine;

namespace Assets.Scripts.Enemies
{
    [CreateAssetMenu(fileName = "NewEnemyType", menuName = "Enemies/New Enemy Type", order = 1)]
    public class EnemyType : ScriptableObject
    {
        #region Serialized Fields
        [SerializeField, Range(1, 1000)]
        private float maxHealth = 10;

        [SerializeField, Range(0.1f, 10f)]
        private float movementSpeed = 5;

        [SerializeField,Range(1, 5)]
        private int scorePerHit=1;

        [SerializeField, Range(1, 10)]
        private int scorePerKill=5;
        #endregion

        #region Public Properties
        public float MaxHealth => maxHealth;
        public float MovementSpeed => movementSpeed;
        public int ScorePerHit => scorePerHit;
        public int ScorePerKill => scorePerKill;
        #endregion
    }
}
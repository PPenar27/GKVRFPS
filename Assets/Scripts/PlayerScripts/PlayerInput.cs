﻿using System.Linq;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.PlayerScripts
{
    public class PlayerInput : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private UnityEvent onUserTap;
        #endregion

        #region Public Properties
        public UnityEvent OnUserTap => onUserTap;
        #endregion

        #region Unity Messages
        private void Update()
        {
            if (Input.anyKeyDown)
            {
                UserTap();
            }

            Input.touches.ToList().ForEach(ProcessTouch);
        }
        #endregion

        #region Private Methods
        private void ProcessTouch(Touch touch)
        {
            if (touch.phase == TouchPhase.Began)
            {
                UserTap();
            }
        }

        private void UserTap()
        {
            onUserTap?.Invoke();
        }
        #endregion
    }
}
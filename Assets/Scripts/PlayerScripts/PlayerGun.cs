﻿using Assets.Scripts.Gameplay;
using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.PlayerScripts
{
    [RequireComponent(typeof(GameObjectPool))]
    public class PlayerGun : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private Transform bulletSpawnPoint;
        #endregion

        #region Private Fields
        private GameObjectPool bulletPool;
        #endregion

        #region Unity Messages
        private void Start()
        {
            bulletPool = GetComponent<GameObjectPool>();
        }
        #endregion

        #region Public Methods
        public void Shoot()
        {
            CreateBullet();
        }
        #endregion

        #region Private Methods
        private void CreateBullet()
        {
            Bullet bulletInstance = (Bullet)bulletPool.GetFromPool();
            bulletInstance.transform.position = bulletSpawnPoint.position;
            bulletInstance.transform.forward = bulletSpawnPoint.forward;
            bulletInstance.Launch();
        }
        #endregion
    }
}

﻿using Assets.Scripts.Managers;
using UnityEngine;
using UnityEngine.Events;

namespace Assets.Scripts.PlayerScripts
{
    public class Player : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private PlayerGun playerGun;
        [SerializeField]
        private SpriteRenderer playerDamageIndicator;
        [SerializeField]
        private Color damageIndicatorNormal;
        [SerializeField]
        private Color damageIndicatorWarning;
        [SerializeField, Range(50, 200)]
        private float playerHealth = 100;
        [SerializeField]
        private float damagePerSecond = 5;
        [SerializeField]
        private UnityEvent onPlayerDamage;
        #endregion

        #region Private Fields
        private float currentHealth;
        #endregion

        #region Public Properties
        public UnityEvent OnPlayerDamage => onPlayerDamage;
        public float GetPlayerHealth01 => currentHealth / playerHealth;
        #endregion

        #region Unity Messages
        private void Start()
        {
            currentHealth = playerHealth;
        }

        private void Update()
        {
            if (Physics.OverlapSphere(transform.position, 5f, LayerMask.GetMask("Enemy")).Length > 0)
            {
                DealDamage(damagePerSecond * Time.deltaTime);
                playerDamageIndicator.color = damageIndicatorWarning;
            }
            else
            {
                playerDamageIndicator.color = damageIndicatorNormal;
            }
        }
        #endregion

        #region Public Methods
        public void Shoot()
        {
            playerGun.Shoot();
        }
        #endregion

        #region Private Methods
        private void DealDamage(float damageValue)
        {
            currentHealth -= damageValue;
            if (currentHealth <= 0)
            {
                GameManager.Instance.EndGame();
            }
            onPlayerDamage.Invoke();
        }
        #endregion
    }
}
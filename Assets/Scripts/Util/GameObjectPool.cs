﻿using System.Collections.Generic;
using System.Linq;
using Assets.Scripts.Interfaces;
using UnityEngine;

namespace Assets.Scripts.Util
{
    public class GameObjectPool : MonoBehaviour
    {
        #region Serialized Fields
        [SerializeField]
        private List<GameObject> objectPrefabs;

        [SerializeField, Range(0, 100)]
        private int precreatedObjectsCount = 10;
        #endregion

        #region Private Fields
        private Queue<IPoolable> gameObjectPool;
        private List<IPoolable> activeInstances;
        #endregion

        #region Public Properties
        public int ActiveInstanceCount => activeInstances.Count;
        #endregion

        #region Unity Messages
        private void Start()
        {
            gameObjectPool = new Queue<IPoolable>();
            activeInstances = new List<IPoolable>();
            PrecreateObjects();
        }
        #endregion

        #region Public Methods
        public void ReturnToPool(IPoolable poolableObject)
        {
            activeInstances.Remove(poolableObject);
            AddToPool(poolableObject);
        }

        public void AddToPool(IPoolable poolableObject)
        {
            poolableObject.GetGameObject().SetActive(false);
            gameObjectPool.Enqueue(poolableObject);
        }

        public IPoolable GetFromPool()
        {
            IPoolable poolableObject = GetNextFromPool() ?? CreateNewInstance();
            poolableObject.ResetProperties();
            activeInstances.Add(poolableObject);
            return poolableObject;
        }
        #endregion

        #region Private Methods
        private void PrecreateObjects()
        {
            for (int i = 0; i < precreatedObjectsCount; i++)
            {
                AddToPool(CreateNewInstance());
            }
        }

        private IPoolable CreateNewInstance()
        {
            GameObject objectPrefab = GetRandomPrefab();
            GameObject createdInstance = Instantiate(objectPrefab);
            IPoolable poolable = createdInstance.GetComponent<IPoolable>();
            poolable.SetSourcePool(this);
            return poolable;
        }

        private IPoolable GetNextFromPool() => gameObjectPool.Count > 0 ? gameObjectPool.Dequeue() : null;
        private GameObject GetRandomPrefab() => objectPrefabs.ElementAt(Random.Range(0, objectPrefabs.Count));
        #endregion
    }
}
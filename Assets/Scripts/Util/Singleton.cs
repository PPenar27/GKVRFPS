﻿using UnityEngine;

namespace Assets.Scripts.Util
{
    public class Singleton< T > : MonoBehaviour where T : MonoBehaviour
    {
        #region  Constants
        protected static T instance;
        #endregion

        #region Public Properties
        public static T Instance {
            get
            {
                if (instance == null)
                {
                    instance = (T)FindObjectOfType(typeof(T));

                    if (instance == null)
                    {
                        Debug.LogError("An instance of " + typeof(T) + " is needed in the scene, but there is none.");
                    }
                    else
                    {
                        (instance as Singleton<T>).Initiate();
                    }
                }

                return instance;
            }
        }
        #endregion

        #region Unity Messages
        protected void Awake()
        {
            CheckInstance(this as T);
            Initiate();
        }
        #endregion

        #region Protected Methods
        protected virtual void Initiate()
        {
        }
        #endregion

        #region Private Methods
        private void CheckInstance(T thisInstance)
        {
            if (instance == null)
            {
                instance = thisInstance;
            }
            else if (instance != thisInstance)
            {
                DestroyImmediate(thisInstance);
            }
        }
        #endregion
    }
}
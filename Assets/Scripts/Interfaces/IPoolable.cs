﻿using Assets.Scripts.Util;
using UnityEngine;

namespace Assets.Scripts.Interfaces
{
    public interface IPoolable
    {
        #region Unity Messages
        void ResetProperties();
        #endregion

        #region Public Methods
        void Launch();

        void SetSourcePool(GameObjectPool pool);

        void ReturnToPool();

        GameObject GetGameObject();
        #endregion
    }
}